﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lanzar : MonoBehaviour {

    public Rigidbody2D rb;
    public GameObject canon;
    bool lanzado = false;
    public int x = 5, y = 5;
    // Use this for initialization
    void Start () {
        rb = (Rigidbody2D)FindObjectOfType(typeof(Rigidbody2D));
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Move()
    {
        if(lanzado== false)
        {
            rb.AddForce(new Vector2(x, y), ForceMode2D.Force);
            //lanzado = true;
        }
        
    }
}
