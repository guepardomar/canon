﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour {

    public Rigidbody2D rb;
    public GameObject canon, anguloSlider, spawnPoint, prefabToSpawn, botonLanzar;
    public Text anguloText, tiempoText, alturaYText, distanciaXText, Vox, Voy;
    bool lanzado = false;
    private int fuerzaX, fuerzaY, 
        numAngGrados = 0, velIni = 10;
    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        numAngGrados = (int)(anguloSlider.GetComponent<Slider>().value);//obtine el angulo
        anguloText.text = anguloSlider.GetComponent<Slider>().value.ToString();//muetra el angulo en pantalla
        canon.transform.eulerAngles =
            new Vector3(0, 0, numAngGrados -10);//Rota el cañon
        if (numAngGrados == 45)//calcula la fuerza adecuada segun el angulo
        {
            fuerzaX = 180;
            fuerzaY = 180;
        }else if (numAngGrados >= 45)
        {
            int a = numAngGrados - 45;
            fuerzaX = 180 - (a * 4); 
            fuerzaY = 180 + (a * 2);
        }else if (numAngGrados <= 45)
        {
            int b = 45 - numAngGrados;
            fuerzaX = 180 + (b * 2);
            fuerzaY = 180 - (b * 4);
        }
        Vox.text = CalcVox().ToString() + " m/s";
        Voy.text = CalcVoy().ToString() + " m/s";
    }

    //Aparece la bola y calcula donde
    void SpawnABall()
    {
        GameObject newObject = Instantiate(prefabToSpawn);
        newObject.transform.position = spawnPoint.transform.position;
    }

    public float CalcVox()
    {
        float anguloRad = numAngGrados * Mathf.Deg2Rad;
        float velX = (velIni * Mathf.Cos(anguloRad));
        if (numAngGrados == 90) { return 0; }
        else { return velX; }
    }

    public float CalcVoy()
    {
        float anguloRad = numAngGrados * Mathf.Deg2Rad;
        float velY = (velIni * Mathf.Sin(anguloRad));
        return velY;
    }

    public float CalcTiempo()
    { 
        float tiempo = (float)(CalcVoy() / 9.8);
        return (tiempo + (float)0.01) * 2 ;
    }

    public float calcYMax()
    {
        double Vo = CalcVoy(), t = CalcTiempo()/2;
        double parte1 = Vo * t;
        double parte2 = 0.5 * -9.8 * (t * t);
        double yMax = parte1 + parte2;
        if (numAngGrados == 0) { return 0; }
        else { return (float)yMax; }
        
    }

    public float calcDistX()
    {
        double Vo = CalcVox(), t = CalcTiempo();
        double x = Vo * t;
        if (numAngGrados == 90) { return 0; }
        else { return (float)x; }

    }

    public void Move()
    {
        if(lanzado == false)
        {
            SpawnABall();
            rb = (Rigidbody2D)FindObjectOfType(typeof(Rigidbody2D));//encuentra la bola
            rb.AddForce(new Vector2(fuerzaX, fuerzaY), ForceMode2D.Force); //aplica velocidad
            lanzado = true;
            tiempoText.text = CalcTiempo().ToString() + " seg";
            alturaYText.text = calcYMax().ToString() + " m";
            distanciaXText.text = calcDistX().ToString() + " m";
            botonLanzar.GetComponentInChildren<Text>().text = "Reiniciar";
        }
        else
        {
            Application.LoadLevel(Application.loadedLevel);
        }
        
    }
}
